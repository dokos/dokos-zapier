[Zapier](https://zapier.com) App to automate [dokos](https://dokos.io).
Deploy this app to Zapier and connect thousands of other apps to your ERP-System, for example, Google Forms, Calendly or Stripe.
Forked from Alyf's [erpnext-zapier](https://github.com/alyf-de/erpnext-zapier) 

![Zapier Screenshot](/img/zap_trigger.png)

# Getting started

* Clone this repository,
* Install the [Zapier CLI](https://zapier.com/developer/documentation/v2/getting-started-cli/#installing-the-cli):

```bash
    npm install -g zapier-platform-cli
    zapier login
```

* Install dependencies

```bash
    npm install
```
* [Deploy this app](https://zapier.com/developer/documentation/v2/getting-started-cli/#deploying-an-app)

```bash
    zapier push
```

# Set up oAuth in dokos

Please follow Frappe's guide on [How to set up oAuth](https://frappe.io/docs/user/en/guides/integration/how_to_set_up_oauth) in dokos.

If this is your first time configuring oAuth you'll need to set your second-level-domain in dokos. For example, if your instance runs at https://erp.domain.tld, use https://domain.tld (without the "erp" subdomain). In dokos, go to `Setup > Integrations > Social Login Keys`, enter your second-level-domain in the field called "Frappe Server URL" and hit save. (If you don't do this you will later get a 417 ValidationError in Zapier.)

Next, run `zapier describe` in the app folder to find this output: 

```
Authentication

┌────────┬───────────────────────────────────────────────────────────────┬────────────────────────────────────────────────┐
│ Type   │ Redirect URI                                                  │ Available Methods                              │
├────────┼───────────────────────────────────────────────────────────────┼────────────────────────────────────────────────┤
│ oauth2 │ https://zapier.com/dashboard/auth/oauth/return/App0000CLIAPI/ │ authentication.test                            │
│        │                                                               │ authentication.oauth2Config.getAccessToken     │
│        │                                                               │ authentication.oauth2Config.refreshAccessToken │
└────────┴───────────────────────────────────────────────────────────────┴────────────────────────────────────────────────┘
```

In dokos, go to `Setup > Integrations > OAuth Client` and click **New**. (You need to have the *System Manager* role.)

To add a client for Zapier, fill in the following details:

1. App Name: Zapier (or whatever you like)
2. Redirect URIs: use the Redirect URI from the output above.
3. Default Redirect URIs: use the Redirect URI from the output above.

Leave all other fields to default and hit save.

# Lets go!

Your app is ready to start. Head over to zapier.com and [create a new Zap](https://zapier.com/app/editor/).

### Compatibility

* Tested on dokos Version 1
* Dates must be passed as strings in ISO format: `YYYY-MM-DD`

### License

Copyright (C) 2018 Raffael Meyer <raffael@alyf.de>
Copyright (c) 2019 Dokos SAS <hello@dokos.io>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
